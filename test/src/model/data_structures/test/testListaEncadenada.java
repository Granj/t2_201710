package model.data_structures.test;

import junit.framework.TestCase;
import model.data_structures.ListaEncadenada;
import model.vo.VOPelicula;

public class testListaEncadenada<T> extends TestCase {
	private VOPelicula pelicula1;
	private VOPelicula pelicula2;
	private VOPelicula pelicula3;
	private VOPelicula pelicula4;
	private ListaEncadenada<T> listO;
	
	public void SetUpEscenario1(){
		listO = new ListaEncadenada<T>();
	}
	public void SetUpEscenario2(){
		listO = new ListaEncadenada<>();
		listO.agregarElementoFinal((T) pelicula1);
	}
	
	public void SetUpEscenario3(){
		listO = new ListaEncadenada<>();
		listO.agregarElementoFinal( (T) pelicula1);
		listO.agregarElementoFinal((T) pelicula2);
		listO.agregarElementoFinal((T) pelicula3);
		listO.agregarElementoFinal((T) pelicula4);
	}
	
	public void testAgregarElementoFinal(){
		SetUpEscenario1();
		listO.agregarElementoFinal(null);
		assertNull(listO.darElementoPosicionActual());
		
		SetUpEscenario2();
		listO.agregarElementoFinal((T) pelicula2);
		assertEquals(pelicula1, listO.darElementoPosicionActual());
		listO.avanzarSiguientePosicion();
		assertEquals(pelicula2, listO.darElementoPosicionActual());
		
	}
	
	public void  testDarNumeroElementos(){
		SetUpEscenario1();
		assertEquals(0, listO.darNumeroElementos());
		
		SetUpEscenario2();
		assertEquals(1, listO.darNumeroElementos());
		
	}
	public void testRetrocederPosicionAnterior(){
		SetUpEscenario1();
		assertFalse(listO.retrocederPosicionAnterior());
		
		SetUpEscenario2();
		assertFalse(listO.retrocederPosicionAnterior());
		
		SetUpEscenario3();
		listO.avanzarSiguientePosicion();
		listO.avanzarSiguientePosicion();
		assertEquals(pelicula2, listO.darElementoPosicionActual());
 		
		
		
	}
	

}
