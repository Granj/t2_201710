package model.data_structures;

public class NodoListaS<T> {
	
	private NodoListaS<T> siguiente;
	
	private T elemento;
	
	public NodoListaS(T elem){
		elemento= elem;
		siguiente=null;
	}
	
	public NodoListaS<T> darSiguiente(){
		return siguiente;
	}
	
	
	public T darElemento(){
		return elemento;
	}
	public void cambiarSiguiente(NodoListaS<T> pNodo){
		siguiente=pNodo;
	}

}
