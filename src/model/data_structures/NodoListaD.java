package model.data_structures;

public class NodoListaD<T> {
	
	private NodoListaD<T> anterior;
	
	private NodoListaD<T> siguiente;
	
	private T elemento;
	
	public NodoListaD(T elem){
		elemento= elem;
		anterior=null;
		siguiente=null;
	}
	
	public NodoListaD<T> darSiguiente(){
		return siguiente;
	}
	
	public NodoListaD<T> darAnterior(){
		return anterior;
	}
	
	public T darElemento(){
		return elemento;
	}
	public void cambiarSiguiente(NodoListaD<T> pNodo){
		siguiente=pNodo;
	}
	public void cambiarAnterior(NodoListaD<T> pNodo){
		anterior=pNodo;
	}
}
