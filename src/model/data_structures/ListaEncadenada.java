package model.data_structures;

import java.util.Iterator;

public class ListaEncadenada<T> implements ILista<T> {
	private NodoListaS<T> primer;
	private int cantidadEle;
	private NodoListaS<T> end;
	private NodoListaS<T> actual;
	
	public  ListaEncadenada() {
		// TODO Auto-generated constructor stub
		primer=null;
		end=null;
		actual=null;
		cantidadEle=0;
		
	}
 
	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		actual = primer;
		Iterator<T> iterator= new Iterator<T>() 
		{

			@Override
			public boolean hasNext() {
				// TODO Auto-generated method stub
				return actual.darSiguiente()==null?false:true;
			}

			@Override
			public T next() {
				// TODO Auto-generated method stub
				actual = actual.darSiguiente();
				return (T)actual;
			}

		};

		return iterator;
	}

	@Override
	public void agregarElementoFinal(T elem) {
		// TODO Auto-generated method stub
		if(primer==null){
			primer=new NodoListaS<T>(elem);
			end= primer;
			actual=primer;
			cantidadEle++;
		}
		else{
			NodoListaS<T> aux = new NodoListaS<T>(elem);
			end.cambiarSiguiente(aux);
			end=aux;
			cantidadEle++;
		}
		
	}

	@Override
	public T darElemento(int pos) {
		// TODO Auto-generated method stub
		Iterator<T> a = iterator();
		int b = pos;
		if(primer!=null){
			while(a.hasNext()){
				NodoListaS<T> aux =(NodoListaS)a.next();
				if(b==0){
					return aux.darElemento();
				}
			}
		}
		return null;
	}


	@Override
	public int darNumeroElementos() {
		// TODO Auto-generated method stub
		return cantidadEle;
	}

	@Override
	public T darElementoPosicionActual() {
		// TODO Auto-generated method stub
		return actual.darElemento();
	}

	@Override
	public boolean avanzarSiguientePosicion() {
		// TODO Auto-generated method stub
		Iterator<T> a = iterator();
		if(a.hasNext()==true){
			a.next();
			return true;
		}
		else{
			actual=primer;
			return false;
		}
	}

	@Override
	public boolean retrocederPosicionAnterior() {
		// TODO Auto-generated method stub
		if(this!=null&&primer!=null){
			T aux =actual.darElemento();
			actual= primer;
			Iterator<T> a = iterator();
			while(a.hasNext()){
				NodoListaS aux1 = (NodoListaS)a.next();
				if(aux1.equals(aux)){
					return true;
				}
			}
			return false;
		}
		return false;
	}
	
}
