package model.data_structures;

import java.util.Iterator;

public class ListaDobleEncadenada<T> implements ILista<T> {
	
	private NodoListaD<T> primer;
	private int cantidadEle;
	private NodoListaD<T> end;
	private NodoListaD<T> actual;
	
	public ListaDobleEncadenada() {
		// TODO Auto-generated constructor stub
		primer = null;
		end=null;
		actual=null;
		cantidadEle=0;
		
	}

	public NodoListaD<T> darNodoActual()
	{
	return actual;
	}
	
	public void cambiarPrimero(T nuevoPrimero)
	{
	 primer	= (NodoListaD<T>) nuevoPrimero;
	}
	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		actual = primer;
		Iterator<T> iterator= new Iterator<T>() 
		{

			@Override
			public boolean hasNext() {
				// TODO Auto-generated method stub
				return actual.darSiguiente()==null?false:true;
			}

			@Override
			public T next() {
				// TODO Auto-generated method stub
				actual = actual.darSiguiente();
				return (T)actual;
			}

			@Override
			public void remove() {
				// TODO Auto-generated method stub
				
			}
		};
		return iterator;
	}

	@Override
	public void agregarElementoFinal(T elem) {
		// TODO Auto-generated method stub
		if(primer==null){
			primer=new NodoListaD<T>(elem);
			end= primer;
			actual=primer;
			cantidadEle++;
		}
		else{
			NodoListaD<T> aux = new NodoListaD<T>(elem);
			aux.cambiarAnterior(end);
			end.cambiarSiguiente(aux);
			end=aux;
			cantidadEle++;
		}
		
	}
	
 public void agregarElemento(T elem)
 {
	 if(primer ==  null)
	 {
		 primer = new NodoListaD<T>(elem);
	 }
	 else
		 {
		 
	 }
 }
 
	@Override
	public T darElemento(int pos) {
		// TODO Auto-generated method stub
		Iterator<T> a = iterator();
		if(primer!=null){
			while(a.hasNext()){
				NodoListaD<T> aux =(NodoListaD)a.next();
			}
		}
		return null;
		
	}


	@Override
	public int darNumeroElementos() {
		// TODO Auto-generated method stub
		return cantidadEle;
	}

	@Override
	public T darElementoPosicionActual() {
		// TODO Auto-generated method stub
		return actual.darElemento();
	}
	

	@Override
	public boolean avanzarSiguientePosicion() {
		// TODO Auto-generated method stub
		Iterator<T> a = iterator();
		if(a.hasNext()==true){
			a.next();
			return true;
		}
		else{
			actual=primer;
			return false;
		}
		
	}

	@Override
	public boolean retrocederPosicionAnterior() {
		// TODO Auto-generated method stub
		if(actual.darAnterior()==null){
			actual=end;
			return false;
		}
		else{
			actual = actual.darAnterior();
			return true;
		}
		
	}

}
