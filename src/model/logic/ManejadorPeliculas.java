package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Iterator;

import model.data_structures.ILista;
import model.data_structures.ListaDobleEncadenada;
import model.data_structures.ListaEncadenada;
import model.data_structures.NodoListaD;
import model.vo.VOAgnoPelicula;
import model.vo.VOPelicula;
import api.IManejadorPeliculas;

public class ManejadorPeliculas implements IManejadorPeliculas {

	private ListaEncadenada<VOPelicula> misPeliculas;

	private ListaDobleEncadenada<VOAgnoPelicula> peliculasAgno;


	@Override
	public void cargarArchivoPeliculas(String archivoPeliculas) 
	{
		// TODO Auto-generated method stub
		File archivo = new File(archivoPeliculas);
		try
		{
			BufferedReader lector = new BufferedReader(new FileReader(archivo));
			String lineaActual = lector.readLine();
			while(lineaActual != null )
			{
				String[] peliculaActual = lineaActual.split(",");
				VOPelicula peliculaAgregando = new VOPelicula();
				String TituloYAgno = "";
				for(int i = 1; i < peliculaActual.length -1; i++ )
				{
					TituloYAgno = TituloYAgno + peliculaActual[i];
				}
				String[] tituloConAgnoParaSeparar = TituloYAgno.split("(");
				String titulo = "";
				for(int j= 0; j < tituloConAgnoParaSeparar.length - 1 ; j++)
				{
					if( j == 0 )
					{
						titulo= titulo + tituloConAgnoParaSeparar[j];  
					}
					else  
					{
						titulo = titulo + "(" + tituloConAgnoParaSeparar[j];	
					}
				}
				String[] agnoArreglado = tituloConAgnoParaSeparar[tituloConAgnoParaSeparar.length -1 ].split(")");
				int agno = Integer.parseInt(agnoArreglado[0]); 	
				ListaEncadenada<String> listaGenerosAsociados = new ListaEncadenada<String>();
				String[] listaAuxiliar = peliculaActual[peliculaActual.length -1].split("|");
				for(int k = 0; k < listaAuxiliar.length ; k ++)
				{
					listaGenerosAsociados.agregarElementoFinal(listaAuxiliar[k]);
				}
				peliculaAgregando.setTitulo(titulo);
				peliculaAgregando.setAgnoPublicacion(agno);
				peliculaAgregando.setGenerosAsociados(listaGenerosAsociados);
				misPeliculas.agregarElementoFinal(peliculaAgregando);   
				Iterator<VOAgnoPelicula> iterador = peliculasAgno.iterator();
				while(iterador.hasNext())
				{

					VOAgnoPelicula nodoIterador = (VOAgnoPelicula) iterador.next();
					if(peliculasAgno.darNumeroElementos() == 0)
					{
						ListaEncadenada<VOPelicula> listaTemporal = new ListaEncadenada<VOPelicula>() ;
						listaTemporal.agregarElementoFinal(peliculaAgregando);
						VOAgnoPelicula agnoAgregando =  new VOAgnoPelicula();
						agnoAgregando.setAgno(agno);
						agnoAgregando.setPeliculas(listaTemporal);
						peliculasAgno.agregarElementoFinal(agnoAgregando);
					}
					else if(nodoIterador.getAgno() == agno )
					{
						ListaEncadenada<VOPelicula> listaTemporal = new ListaEncadenada<VOPelicula>() ;
						Iterator<VOPelicula> iterador2 =nodoIterador.getPeliculas().iterator();
						while(iterador2.hasNext())
						{
							listaTemporal.agregarElementoFinal((VOPelicula) iterador2.next());
						}
						listaTemporal.agregarElementoFinal(peliculaAgregando);
						nodoIterador.setPeliculas(listaTemporal);
					}
					else if( agno < nodoIterador.getAgno())
					{
						ListaEncadenada<VOPelicula> listaTemporal = new ListaEncadenada<VOPelicula>() ;
						listaTemporal.agregarElementoFinal(peliculaAgregando);
						VOAgnoPelicula agregando=  new VOAgnoPelicula();
						agregando.setAgno(agno);
						agregando.setPeliculas(listaTemporal);
						NodoListaD<VOAgnoPelicula> agnoAgregando =  new NodoListaD<VOAgnoPelicula>(agregando);
						peliculasAgno.darNodoActual().cambiarAnterior( agnoAgregando);
						peliculasAgno.retrocederPosicionAnterior();
						peliculasAgno.darNodoActual().cambiarSiguiente(agnoAgregando);
					}
				}	
				peliculasAgno.avanzarSiguientePosicion();
			}
			lector.close();

		}
		catch(Exception e)
		{
			System.out.println("Error fatal, no se pudo leer el archivo, por favor reinicie el programa");
		}

	}

	@Override
	public ILista<VOPelicula> darListaPeliculas(String busqueda) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ILista<VOPelicula> darPeliculasAgno(int agno) {


		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public VOAgnoPelicula darPeliculasAgnoSiguiente() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public VOAgnoPelicula darPeliculasAgnoAnterior() {
		// TODO Auto-generated method stub
		return null;
	}

}
